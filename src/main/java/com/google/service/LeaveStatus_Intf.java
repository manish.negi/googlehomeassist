package com.google.service;

public interface LeaveStatus_Intf
{
	public String getLeaveStatus(String ssoId);

}
