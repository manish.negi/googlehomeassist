package com.google.serviceimpl;
import org.springframework.stereotype.Service;

import com.google.service.BizUpdate_Intf;

@Service
public class BizUpdate implements BizUpdate_Intf
{
	public String getBizUpdate()
	{
		return " As of 20-SEP-2018, the Business update for MLI is : Adj MFYP MTD : 148.06 Cr "
				+ "Applied AFYP MTD: 196.46 Cr WIP Adj MFYP: 67.50 Cr.  ";
	}

}
