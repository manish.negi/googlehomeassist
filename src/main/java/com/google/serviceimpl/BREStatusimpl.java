package com.google.serviceimpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.stereotype.Service;

import com.google.service.BREStatus_Intf;

@Service
public class BREStatusimpl implements BREStatus_Intf
{

	public String getBREStatus(String ssoId)
	{
		StringBuilder result = new StringBuilder();
		String output = new String();
		ResourceBundle res = ResourceBundle.getBundle("application");
		String soaCorrelationId = "CorelationId"+System.currentTimeMillis();
		HttpURLConnection conn = null;
		try {
			String extURL = res.getString("BREBalance");
			URL url = new URL(extURL);
			conn = (HttpURLConnection) url.openConnection();
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			StringBuilder requestdata = new StringBuilder();

			requestdata.append("	{	");
			requestdata.append("	  \"request\": {	");
			requestdata.append("	    \"header\": {	");
			requestdata.append("	      \"soaCorrelationId\": \""+soaCorrelationId+"\",	");
			requestdata.append("	      \"soaMsgVersion\": \"1.0\"	");
			requestdata.append("	    },	");
			requestdata.append("	    \"requestData\": {	");
			requestdata.append("	      \"empId\": \"HOM2801\",	");
			requestdata.append("	      \"authToken\": \"qgAAAAQDAgEBAAAAvAIAAAAAAAAsAAAABABTaGRyAk4Acwg4AC4AMQAwABQtVdBsApQNhHqGcEzBO1MyVwciVmoAAAAFAFNkYXRhXnicJYs9DkBAGETfLlEq3IPYjb8L+KlE0GsUEhGF6zmcb9cU7yUzmQcIA62U+NX4JCc7BzcXloYcQ9Qy0hNPLHSsbAzMFNaPNan4Zyl0TSbXSmjEmfRuKYUFfOsfDI0=\"	");
			requestdata.append("	    }	");
			requestdata.append("	  }	");
			requestdata.append("	}	");

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(requestdata.toString());
			writer.flush();
			try {
				writer.close();
			} catch (Exception e1) {
			}

			int apiResponseCode = conn.getResponseCode();
			if (apiResponseCode == 200) 
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
			else
			{

				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception Occoured While Calling API's " + e);
		}
		return result.toString();
	}
}
