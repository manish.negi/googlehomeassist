package com.google.serviceimpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.stereotype.Service;

import com.google.service.SSOAuthenticate_Intf;
@Service
public class SSOAuthenticateimpl implements SSOAuthenticate_Intf
{
	public String getUserDetail(String ssoId)
	{
		StringBuilder result = new StringBuilder();
		String output = new String();
		ResourceBundle res = ResourceBundle.getBundle("application");
		HttpURLConnection conn = null;
		try {
			String extURL = res.getString("GetUserDetail");
			URL url = new URL(extURL);
			conn = (HttpURLConnection) url.openConnection();
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			StringBuilder requestdata = new StringBuilder();

			requestdata.append("	{	");
			requestdata.append("	  \"Request\": {	");
			requestdata.append("	    \"RequestInfo\": {	");
			requestdata.append("	      \"Transactions\": [	");
			requestdata.append("	        {	");
			requestdata.append("	         \"ssoId\":\""+ssoId.replaceAll("\\s+","").toUpperCase()+"\"	");
			requestdata.append("	        }	");
			requestdata.append("	      ]	");
			requestdata.append("	    }	");
			requestdata.append("	  }	");
			requestdata.append("	}	");

			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(requestdata.toString());
			writer.flush();
			try {
				writer.close();
			} catch (Exception e1) {
			}
			int apiResponseCode = conn.getResponseCode();
			System.out.println("GET-User-Detail APIResponseCode"+apiResponseCode);
			if (apiResponseCode == 200) 
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
			else
			{

				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception Occoured While Calling API's " + e);
		}
		
		return result.toString();
	}

}
