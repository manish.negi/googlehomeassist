package com.google.serviceimpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

import javax.net.ssl.HttpsURLConnection;

import org.springframework.stereotype.Service;

import com.google.service.LeaveStatus_Intf;

@Service
public class Leave_status_impl implements LeaveStatus_Intf 
{
	public String getLeaveStatus(String ssoId)
	{
		ResourceBundle res = ResourceBundle.getBundle("application");
		StringBuilder result = new StringBuilder();
		String output = new String();
		HttpURLConnection conn = null;
		int pinTakeNumber=250292;
		int empRec=0;
		try {
			String extURL = res.getString("LeaveStatus");
			URL url = new URL(extURL);
			conn = (HttpURLConnection) url.openConnection();
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			StringBuilder requestdata = new StringBuilder();

			requestdata.append("	{	");
			requestdata.append("	  \"request\": {	");
			requestdata.append("	    \"header\": {	");
			requestdata.append("	      \"soaCorrelationId\": \"VRx92uoQfU\",	");
			requestdata.append("	      \"soaMsgVersion\": \"1.0\",	");
			requestdata.append("	      \"soaAppId\": \"EAPP\",	");
			requestdata.append("	      \"soaUserId\": \"EAPPPROD123\",	");
			requestdata.append("	      \"soaPassword\": \"TWF4dHJ2bA==\"	");
			requestdata.append("	    },	");
			requestdata.append("	    \"requestData\": {	");
			requestdata.append("	      \"GetBalances\": {	");
			requestdata.append("	      \"empId\": \"HOM2801\",	");
			requestdata.append("	        \"empRec\": "+empRec+",	");
			requestdata.append("	        \"pinTakeNumber\": "+pinTakeNumber+",	");
			requestdata.append("	        \"asOfDate\": \"2018-01-27\",	");
			requestdata.append("	        \"token\": \"qwAAAAQDAgEBAAAAvAIAAAAAAAAsAAAABABTaGRyAk4Acwg4AC4AMQAwABSYhSVnjROWSxusAcKWF84cTBpm82sAAAAFAFNkYXRhX3icHYo7CoBADAVnVSwtvMcuuoqfA/ipRNTexkIQsfB6Hs6QQDKPyXuBKAyMEX4BOunFwcnDjachIyfumBhIZlZ6NnZGFkqvzwartLKV3hpHIXTiC82ltLz4Fn7qzwyZ\"	");
			requestdata.append("	      }	");
			requestdata.append("	    }	");
			requestdata.append("	  }	");
			requestdata.append("	}	");

			
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(requestdata.toString());
			writer.flush();
			try {
				writer.close();
			} catch (Exception e1) {
			}

			int apiResponseCode = conn.getResponseCode();
			if (apiResponseCode == 200) 
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
			else
			{

				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception Occoured While Calling Leave Status API " + e);
		}
		return result.toString();
	}
}
