package com.google;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.Logic.AuthenticateLogic_Intf;
import com.google.Logic.BREBalanceLogic_Intf;
import com.google.Logic.BizUpdateLogic_Intf;
import com.google.Logic.DOBLogic_Intf;
import com.google.Logic.LeaveStatusLogic_Intf;
import com.google.Logic.PunchRequestLogic_Intf;
import com.google.common.Response;


@Controller
@RequestMapping("/webhook")
public class GoogleAssistance {

	static Map<String, String> map  = new ConcurrentHashMap<>();
	@Autowired
	private PunchRequestLogic_Intf punchRequestLogic;
	@Autowired
	private BREBalanceLogic_Intf bREBalanceLogic;
	@Autowired
	private BizUpdateLogic_Intf bizUpdateLogic;
	@Autowired
	private LeaveStatusLogic_Intf leaveStatusLogic;
	@Autowired
	AuthenticateLogic_Intf authenticateLogic;
	@Autowired
	private Response response;
	@Autowired
	private DOBLogic_Intf dobLogic;

	String displayName="", dateofJoining="";

	@RequestMapping(method = RequestMethod.POST)
	public @ResponseBody String webhook(@RequestBody String obj)
	{
		System.out.println("Request comes ---"+obj);
		String speech = null;
		String sessionId="";
		String action="";
		String ssoIds="";
		String dobFromUser=""; 
		String input="";

		try {
			System.out.println("first try");
			JSONObject object = new JSONObject(obj.toString());
			System.out.println("Print Object "+object);
			sessionId = object.get("responseId")+"";
			try{
				action = object.getJSONObject("queryResult").get("action")+"";
			}catch(Exception ex)
			{
				action="";
			}
			try{
				dobFromUser = object.getJSONObject("queryResult").getJSONObject("parameters").getJSONArray("date").get(0)+"";
				String str[] = dobFromUser.split("T");
				dobFromUser=str[0];
			}catch(Exception ex)
			{
				dobFromUser="";
			}
			try{
				input = object.getJSONObject("queryResult").getJSONObject("parameters").get("input")+"";
			}catch(Exception ex)
			{
				input="";
			}
			try{
				ssoIds = object.getJSONObject("queryResult").getJSONObject("parameters").get("ssoId")+"";
				System.out.println("---1111----"+ssoIds);
				ssoIds=ssoIds.replaceAll("\\s+","").toUpperCase();
				System.out.println("---2222----"+ssoIds);
			}catch(Exception ex)
			{
				ssoIds="";
			}
			switch(action.toUpperCase())
			{
			case "AUTHENTICATE":
			case "INPUT.WELCOME":
			{
				System.out.println("Testing -- Input.Welcome");
				speech=authenticateLogic.authenciateSSOID(ssoIds, map);
				System.out.println("Testing --second...");
				if(!"Invalid".equalsIgnoreCase(speech))
				{
					String str[] = speech.split("@");
					speech=str[0];
					displayName=str[1];
					dateofJoining=str[2];
				}
				else
				{
					speech="Your SSOID is invalid, Please Validate it again";
				}
			}
			break;
			case "PUNCH_REQUEST":
			{
				speech=punchRequestLogic.punchRequest(ssoIds, map);
			}
			break;
			case "LEAVE_STATUS":
			{
				speech=leaveStatusLogic.leaveStatus(ssoIds, map);
			}
			break;
			case "BRE_STATUS":
			{
				speech=bREBalanceLogic.breBalance(ssoIds, map);
			}
			break;
			case "BIZ_UPDATE":
			{
				String orignalData = bizUpdateLogic.getBizUpdate(ssoIds, map, input);
				if(orignalData!=null && !orignalData.isEmpty())
				{
					speech=orignalData;
				}
				else
				{
					speech="Sorry for inconvenience Please try again";
				}
			}
			break;
			case "DOJ":
			{
				speech=dobLogic.dobCheck(dateofJoining, dobFromUser, displayName);
			}
			break;
			default:
			}
		} 
		catch (Exception e) {
			System.out.println("Exception Occoured");
			speech="We are in Glitch, Please try after sometime";
		}
		if(!"AUTHENTICATE".equalsIgnoreCase(action)&& !"INPUT.WELCOME".equalsIgnoreCase(action)
				&& !"DOJ".equalsIgnoreCase(action))
		{
			speech=speech+". Is there any thing i can help you";
		}
		return response.googleAssistanceResponse(speech);
	}
}
