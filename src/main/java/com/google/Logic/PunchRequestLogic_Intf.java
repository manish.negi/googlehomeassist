package com.google.Logic;

import java.util.Map;

public interface PunchRequestLogic_Intf 
{
	public String punchRequest(String ssoId, Map<String, String> map);

}
