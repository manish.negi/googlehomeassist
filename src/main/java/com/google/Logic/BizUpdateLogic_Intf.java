package com.google.Logic;

import java.util.Map;

public interface BizUpdateLogic_Intf 
{
	public String getBizUpdate(String ssoId, Map<String,String> map, String input);

}
