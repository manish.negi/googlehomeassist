package com.google.Logic;

public interface DOBLogic_Intf {
	public String dobCheck(String dobFromService, String dobFromUser, String displayName);
}
