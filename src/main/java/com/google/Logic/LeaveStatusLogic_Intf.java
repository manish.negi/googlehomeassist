package com.google.Logic;

import java.util.Map;

public interface LeaveStatusLogic_Intf {
	public String leaveStatus(String ssoId, Map<String,String> map);
}
