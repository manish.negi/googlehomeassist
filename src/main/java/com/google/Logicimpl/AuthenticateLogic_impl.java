package com.google.Logicimpl;

import java.util.Map;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.Logic.AuthenticateLogic_Intf;
import com.google.service.SSOAuthenticate_Intf;

@Service
public class AuthenticateLogic_impl implements AuthenticateLogic_Intf
{
	@Autowired
	private SSOAuthenticate_Intf authenticate;

	String displayName="", speech="", ssoIds="", joiningDate="", identifier="";
	@Override
	public String authenciateSSOID(String ssoId, Map<String, String> map) 
	{
		if(ssoId.contains("2801"))
		{
			ssoId = "KDHOM2801";
		}
		else if(ssoId.contains("1417"))
		{
			ssoId="PTHOM1417";

		}
		else if(ssoId.contains("1164"))
		{
			ssoId="VBHOM1164";
		}
		else if(ssoId.contains("3321"))
		{
			ssoId="MMHOM3321";
		}
		else if(ssoId.contains("0103"))
		{
			ssoId="VVCHE0103";
		}
		else if(ssoId.contains("0520"))
		{
			ssoId="MNHOM0520";
		}
		else if(ssoId.contains("2626"))
		{
			ssoId="SKSHOM2626";
		}
		else if(ssoId.contains("3847"))
		{
			ssoId="VSHOM3847";
		}
		else if(ssoId.contains("4053"))
		{
			ssoId="SAHOM4053";
		}
		else if(ssoId.contains("3698"))
		{
			ssoId="PMHOM3698";
		}
		if(!"VVCHE0103".equalsIgnoreCase(ssoId))
		{
			try{
				map.put("botssoId", ssoId);
				ssoIds=ssoId;
				String [] splits = ssoIds.split("HOM");
				ssoIds=splits[1];
				map.put("ssoId", "HOM"+ssoIds);
			}catch(Exception ex)
			{
				System.out.println("First Catch");
				map.put("ssoId", "");
				speech="Your SSOID is invalid, Please Validate it again";
				identifier="Invalid";
			}
		}
		else
		{
			try{
				if("VVCHE0103".equalsIgnoreCase(ssoId))
				{
					ssoIds="VVCHE0103";
					map.put("ssoId", ssoIds);
					map.put("botssoId", ssoId);
				}
			}catch(Exception ex)
			{
				System.out.println("Second Catch");
				map.put("ssoId", "");
				speech="Your SSOID is invalid, Please Validate it again";
				identifier="Invalid";
			}
		}
		if(!"Invalid".equalsIgnoreCase(identifier))
		{
			String orignalData = authenticate.getUserDetail(ssoId);
			JSONObject objects = new JSONObject(orignalData.toString());
			try{
				String jsonobject = objects.getJSONObject("Response").getJSONObject("ResponseInfo").getJSONArray("Transactions").get(0)+"";
				JSONObject object2 = new JSONObject(jsonobject);
				displayName = object2.get("mnyldisplayname")+"";
				joiningDate = object2.get("mnyljoiningdate")+"";
				System.out.println("Testing ---- D-O-J "+joiningDate);
				speech = "Hello "+displayName +", welcome to Max Life. "
						+ "Your SSO ID is Validated, "
						+ "Please validate your Date of Joining to complete validation";


			}catch(Exception ex)
			{
				speech="Your SSOID is invalid, Please Validate it again";
			}
			return speech+"@"+displayName+"@"+joiningDate;
		}
		return identifier;
	}
}
