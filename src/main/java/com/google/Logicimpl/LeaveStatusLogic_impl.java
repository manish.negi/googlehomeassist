package com.google.Logicimpl;

import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.Logic.LeaveStatusLogic_Intf;
import com.google.service.LeaveStatus_Intf;

@Service
public class LeaveStatusLogic_impl implements LeaveStatusLogic_Intf 
{
	String speech="";
	@Autowired
	private LeaveStatus_Intf leaveStatus;
	@Override
	public String leaveStatus(String ssoId, Map<String,String> map) {
		ssoId=map.get("ssoId");
		String orignalData = leaveStatus.getLeaveStatus(ssoId);
		JSONObject objects = new JSONObject(orignalData.toString());
		try{
			String Balance = objects.getJSONObject("response").getJSONObject("responseData").getJSONObject("GetBalancesResponse").getString("BALANCE")+"";
			String FutureLeaves = objects.getJSONObject("response").getJSONObject("responseData").getJSONObject("GetBalancesResponse").getString("FutureLeaves")+"";
			String Remaining = objects.getJSONObject("response").getJSONObject("responseData").getJSONObject("GetBalancesResponse").getString("Remaining")+"";
			speech = "Your Leave status is : Balance "+Balance+" Future Leaves "+FutureLeaves+ " and " + " Remaning "+Remaining;
		}catch(Exception ex)
		{
			speech = "Hi I am not getting any Leave Status ";
		}
		return speech;
	}

}
