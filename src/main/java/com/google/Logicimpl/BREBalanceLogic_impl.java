package com.google.Logicimpl;

import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.Logic.BREBalanceLogic_Intf;
import com.google.service.BREStatus_Intf;

@Service
class BREBalanceLogic_impl implements BREBalanceLogic_Intf 
{
	@Autowired
	private BREStatus_Intf bREStatus;
	String speech="";
	@Override
	public String breBalance(String ssoId, Map<String, String> map) {
		ssoId=map.get("ssoId");
		String orignalData = bREStatus.getBREStatus(ssoId);
		JSONObject objects = new JSONObject(orignalData.toString());
		try{
			String ClaimType = objects.getJSONObject("response").getJSONObject("responseData").getString("comments")+"";
			speech = "Hi your B R E Status is "+ClaimType;
		}catch(Exception ex)
		{
			speech = "Hi I am not getting any  B R E Status";
		}
		return speech;
	}
}
