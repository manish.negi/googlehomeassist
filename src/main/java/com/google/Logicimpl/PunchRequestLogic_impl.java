package com.google.Logicimpl;

import java.util.Map;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.Logic.PunchRequestLogic_Intf;
import com.google.service.PunchRequest_Intf;

@Service
public class PunchRequestLogic_impl implements PunchRequestLogic_Intf 
{
	@Autowired
	private PunchRequest_Intf punchRequest;
	
	String FinalTime="", speech="", displayName="";
	@Override
	public String punchRequest(String ssoId, Map<String, String> map)
	{
		ssoId=map.get("ssoId");
		System.out.println("PunchRequest---"+ssoId);
		String orignalData = punchRequest.getPunchRequest(ssoId);
		JSONObject objects = new JSONObject(orignalData.toString());
		try{
			String punchTime = objects.getJSONObject("response").getJSONObject("payload").getJSONArray("punchDetails").get(0)+"";
			JSONObject object2 = new JSONObject(punchTime);
			FinalTime = object2.get("punchTime")+"";
			System.out.println("Tesing--------"+FinalTime);
			if(!"".equalsIgnoreCase(FinalTime) && !"null".equalsIgnoreCase(FinalTime))
			{
				speech="Hi " +displayName+ "Your punch time is "+FinalTime+"";
			}
			else{
				speech="Hi "+displayName+ " i am not found any time for you";
			}
		}catch(Exception ex)
		{
			speech="Hi "+displayName+ " i am not found any time for you";
		}
		return speech;
	}
}
