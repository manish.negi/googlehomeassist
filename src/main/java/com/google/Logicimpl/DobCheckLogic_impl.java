package com.google.Logicimpl;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.google.Logic.DOBLogic_Intf;

@Service
public class DobCheckLogic_impl implements DOBLogic_Intf{

	String speech="";
	@Override
	public String dobCheck(String dobFromService, String dobFromUser, String displayName) 
	{
		
		try
		{
			String str2[] = dobFromService.split(" ");
			String joiningDatefinal=str2[0];
			 SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		        Date date1 = sdf.parse(joiningDatefinal);
		        Date date2 = sdf.parse(dobFromUser);

		        System.out.println("date1 : " + sdf.format(date1));
		        System.out.println("date2 : " + sdf.format(date2));
		        if (date1.compareTo(date2) == 0) {
		            speech="Hello " +displayName+" you are Sucessfully Authenticated"
		            		+ " What would you like to do from following menu,"
							+ " Check Business Update, Achivement, Protection, Applied Business, Paid Business, "
							+ "WIP, Growth, Product Mix NOP and so on.";
							
		        } else {
		            speech="Your date of Joining does not matched in with our records, Please re authenticate your date of joining before Proceed";
		        }
		}catch(Exception ex)
		{
			speech="Something Went wrong";
		}
		return speech;
	}
}
