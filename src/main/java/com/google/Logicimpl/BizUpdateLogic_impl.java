package com.google.Logicimpl;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.Logic.BizUpdateLogic_Intf;
import com.google.botIntegration.BPMA_MIS_BOT;
@Service
public class BizUpdateLogic_impl implements BizUpdateLogic_Intf
{
	@Autowired
	private BPMA_MIS_BOT bpma_mis_bot;
	
	String speech="";
	
	public String getBizUpdate(String ssoId, Map<String,String> map, String input)
	{
		ssoId=map.get("botssoId");
		speech=bpma_mis_bot.BPMA_MIS_Integration(ssoId, input);
		return speech;
	}

}



