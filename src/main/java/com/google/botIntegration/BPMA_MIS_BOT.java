package com.google.botIntegration;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

import javax.net.ssl.HttpsURLConnection;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.serviceimpl.BREStatusimpl;

@Service
public class BPMA_MIS_BOT 
{
	
	String sessionId="";
	String speech="", botIdentifier="";
	ResourceBundle res = ResourceBundle.getBundle("application");
	public String BPMA_MIS_Integration(String ssoId, String input)
	{
		StringBuilder result = new StringBuilder();
		String output = new String();
		HttpURLConnection conn = null;
		try {
			String UniqueId = "UniqueId"+System.currentTimeMillis();
			String extURL = res.getString("BPMAMISBOT");
			URL url = new URL(extURL);
			conn = (HttpURLConnection) url.openConnection();
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			String query = ssoId+" "+"authorize_ecube_002_2017_BOT_certificate idiosyncratic_key web ecube";
			StringBuilder requestdata = new StringBuilder(); 
			requestdata.append("	{	");
			requestdata.append("	  \"query\": \""+query+"\",	");
			requestdata.append("	  \"randomString\": \""+UniqueId+"\",	");
			requestdata.append("	  \"botName\": \"EAPPBOT\"	");
			requestdata.append("	}	");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(requestdata.toString());
			writer.flush();
			try {
				writer.close();
			} catch (Exception e1) {
			}
			int apiResponseCode = conn.getResponseCode();
			System.out.println("API Response Code : - " + apiResponseCode);
			if (apiResponseCode == 200) 
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
				System.out.println("External API Call : END");
			}
			else
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception Occoured While Calling API's " + e);
		}
		JSONObject objects = new JSONObject(result.toString());
		try{
			try{
			String jsonObject = objects.getJSONObject("result").getJSONObject("fulfillment").getJSONObject("data")
					.getJSONObject("facebook").getJSONArray("buttons").get(0)+"";
			JSONObject object2 = new JSONObject(jsonObject);
			botIdentifier = object2.get("text")+"";
			}
			catch(Exception ex)
			{
				botIdentifier="NOT_AUTH";
				System.out.println("NOT_Authroize to access the bot");
			}
			if(!"Business Numbers".equalsIgnoreCase(botIdentifier))
			{
				speech="You are not authorize to access M I S Bot";
				return speech;
			}
			sessionId = objects.getString("sessionId")+"";
		}catch(Exception ex)
		{
			System.out.println("Exception Occoured while getting response from bot");
		}
		speech=BPMA_MIS_Integration2(ssoId, sessionId, input);
		return speech;
	}
	public String BPMA_MIS_Integration2(String ssoId, String sessionId, String input)
	{
		StringBuilder result = new StringBuilder();
		String output = new String();
		HttpURLConnection conn = null;
		try {
			String extURL = res.getString("BPMAMISBOT");
			URL url = new URL(extURL);
			conn = (HttpURLConnection) url.openConnection();
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			String query = "misbot_validated_key_bot_2017"+" "+ssoId;
			StringBuilder requestdata = new StringBuilder(); 
			requestdata.append("	{	");
			requestdata.append("	  \"query\": \""+query+"\",	");
			requestdata.append("	  \"randomString\": \""+sessionId+"\",	");
			requestdata.append("	  \"botName\": \"Business Numbers\"	");
			requestdata.append("	}	");
			System.out.println("External API Call : START");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(requestdata.toString());
			writer.flush();
			try {
				writer.close();
			} catch (Exception e1) {
			}

			int apiResponseCode = conn.getResponseCode();
			System.out.println("API Response Code : - " + apiResponseCode);
			if (apiResponseCode == 200) 
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
				System.out.println("External API Call : END");
			}
			else
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					result.append(output);
				}
				conn.disconnect();
				br.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception Occoured While Calling API's " + e);
		}
		JSONObject objects = new JSONObject(result.toString());
		try{
			speech = objects.getJSONObject("result").getJSONObject("fulfillment").getString("speech")+"";
			sessionId = objects.getString("sessionId")+"";
		}catch(Exception ex)
		{
			System.out.println("Exception Occoured while getting response from bot");
		}
		speech=BPMA_MIS_Integration3(sessionId, input);
		return speech;
	}
	public String BPMA_MIS_Integration3(String sessionId, String input)
	{
		System.out.println("Inside Method:: BPMA_MIS_Integration ");
		StringBuilder result2 = new StringBuilder();
		String output = new String();
		HttpURLConnection conn = null;
		try {
			String UniqueId = "UniqueId"+System.currentTimeMillis();
			String extURL = res.getString("BPMAMISBOT");
			URL url = new URL(extURL);	
			conn = (HttpURLConnection) url.openConnection();
			HttpsURLConnection.setFollowRedirects(true);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			StringBuilder requestdata = new StringBuilder(); 
			requestdata.append("	{	");
			requestdata.append("	  \"query\": \""+input+"\",	");
			requestdata.append("	  \"randomString\": \""+sessionId+"\",	");
			requestdata.append("	  \"botName\": \"Business Numbers\"	");
			requestdata.append("	}	");
			System.out.println("External API Call : START");
			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
			writer.write(requestdata.toString());
			writer.flush();
			try {
				writer.close();
			} catch (Exception e1) {
			}
			int apiResponseCode = conn.getResponseCode();
			System.out.println("API Response Code : - " + apiResponseCode);
			if (apiResponseCode == 200) 
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
				while ((output = br.readLine()) != null) {
					result2.append(output);
				}
				conn.disconnect();
				br.close();
				System.out.println("External API Call : END");
			}
			else
			{
				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
				while ((output = br.readLine()) != null) {
					result2.append(output);
				}
				conn.disconnect();
				br.close();
			}
		}
		catch(Exception e)
		{
			System.out.println("Exception Occoured While Calling API's " + e);
		}
		JSONObject objects2 = new JSONObject(result2.toString());
		try{
			speech = objects2.getJSONObject("result").getJSONObject("fulfillment").getString("speech")+"";
			System.out.println("----Final Speech----"+speech);
		}catch(Exception ex)
		{
			System.out.println("Exception Occoured while getting response from bot");
		}
		return speech;
	}
}
