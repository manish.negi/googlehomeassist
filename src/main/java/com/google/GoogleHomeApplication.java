package com.google;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class GoogleHomeApplication {
	public static void main(String[] args) {
		SpringApplication.run(GoogleHomeApplication.class, args);
	}
}
